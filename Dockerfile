FROM debian:buster-slim
COPY ./crab-mock /usr/local/bin/crab-mock
ENTRYPOINT ["/usr/local/bin/crab-mock"]