use serde::de;
use std::fmt;

#[derive(Debug, PartialEq)]
pub enum MatchingValue {
    Matches(String),
    Contains(String),
}

struct MatchingValueVisitor {}

impl <'de> de::Visitor<'de> for MatchingValueVisitor {
    type Value = MatchingValue;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("Header Matcher")
    }

    fn visit_map<M>(self, mut access: M) -> std::result::Result<Self::Value, M::Error>
        where M: de::MapAccess<'de>
    {
        let entry = access.next_entry()?;
        let (key, value): (String, String) = entry.ok_or(de::Error::custom("no valid matcher defined for header"))?;
        match key.as_str() {
            "contains" => 
                Ok(MatchingValue::Contains(value)),
            "matches" =>
                Ok(MatchingValue::Matches(value)),
            unknown => 
                Err(de::Error::custom(format!("unknown matcher found '{}' in header", unknown))),
        }
    }    

    fn visit_str<E>(self, value: &str) -> std::result::Result<Self::Value, E> {
        Ok(MatchingValue::Matches(String::from(value)))
    }
}

impl <'de> de::Deserialize<'de> for MatchingValue {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error> 
        where D: de::Deserializer<'de>
    {
        let visitor = MatchingValueVisitor {};
        deserializer.deserialize_any(visitor)
    }
}


pub struct HeaderMatcherSpec {
    pub name: String,
    pub value: MatchingValue,
}

struct HeaderMatcherSpecVisitor {}

impl <'de> de::Visitor<'de> for HeaderMatcherSpecVisitor {
    type Value = HeaderMatcherSpec;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("Header Specification")
    }

    fn visit_map<M>(self, mut access: M) -> std::result::Result<Self::Value, M::Error>
        where M: de::MapAccess<'de>
    {
        if let Some((name, value)) = access.next_entry()? {
            return Ok(HeaderMatcherSpec {name, value})
        };
        Err(de::Error::custom("no valid matcher defined for header"))
    }
}

impl <'de> de::Deserialize<'de> for HeaderMatcherSpec {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error> 
        where D: de::Deserializer<'de>
    {
        deserializer.deserialize_map(HeaderMatcherSpecVisitor {})
    }
}

#[derive(Debug, PartialEq)]
pub struct HeaderSpec(pub String, pub String);

struct HeaderVisitor {}

impl <'de> de::Visitor<'de> for HeaderVisitor {
    type Value = HeaderSpec;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("Header")
    }

    fn visit_map<M>(self, mut access: M) -> std::result::Result<Self::Value, M::Error>
        where M: de::MapAccess<'de>
    {
        if let Some((name, value)) = access.next_entry()? {
            return Ok(HeaderSpec(name, value))
        };
        Err(de::Error::custom("no valid matcher defined for header"))
    }
}

impl <'de> de::Deserialize<'de> for HeaderSpec {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error> 
        where D: de::Deserializer<'de>
    {
        deserializer.deserialize_map(HeaderVisitor {})
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use MatchingValue::*;

    #[test]
    fn test_header_matcher_simple_form() {
        let input = r#"Accept: application/json"#;
        let spec: HeaderMatcherSpec = serde_yaml::from_str(input).expect("Failed to parse");
        assert_eq!(spec.name, "Accept");
        assert_eq!(spec.value, Matches(String::from("application/json")));
    }

    #[test]
    fn test_header_matcher_map_form() {
        let input = r#"
            Accept: 
                matches: application/json
        "#;
        let spec: HeaderMatcherSpec = serde_yaml::from_str(input).expect("Failed to parse");
        assert_eq!(spec.name, "Accept");
        assert_eq!(spec.value, Matches(String::from("application/json")));
    }

    #[test]
    fn test_header_partial_matching() {
        let input = r#"
            Accept: 
                contains: application/json
        "#;
        let spec: HeaderMatcherSpec = serde_yaml::from_str(input).expect("Failed to parse");
        assert_eq!(spec.name, "Accept");
        assert_eq!(spec.value, Contains(String::from("application/json")));
    }
    
    #[test]
    fn test_header_primitive_spec_for_responses() {
        let input = r#"Accept: application/text"#;
        let HeaderSpec(name, value) = serde_yaml::from_str(input).expect("Failed to parse");
        assert_eq!(name, "Accept");
        assert_eq!(value, "application/text");
    }
}