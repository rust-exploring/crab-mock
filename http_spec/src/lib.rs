mod header;

use serde::{Deserialize};
use std::io::{BufReader, Read};
use std::path::Path;
use std::fs::File;

#[derive(Deserialize)]
pub struct RequestSpec {
    pub method: String,
    pub url: String,
    
    #[serde(default)]
    pub headers: Vec<header::HeaderMatcherSpec>,
}

#[derive(Deserialize)]
pub struct ResponseSpec {
    pub status: u32,
    pub body: String,

    #[serde(default)]
    pub headers: Vec<header::HeaderSpec>,
}

#[derive(Deserialize)]
pub struct Spec {   
    pub request: RequestSpec,
    pub response: ResponseSpec,
}

#[derive(Debug)]
pub enum SpecError {
    ParsingError(serde_yaml::Error),
    IoError(std::io::Error),
}

type Result<T> = std::result::Result<T, SpecError>;

pub fn parse_spec<R>(input: R) -> std::result::Result<Spec, serde_yaml::Error>
    where R: Read
{
    serde_yaml::from_reader(input)
}

pub fn parse_spec_from_file<P>(path: P) -> Result<Spec> 
    where P: AsRef<Path>
{
    let file = File::open(path).map_err(SpecError::IoError)?;
    let reader = BufReader::new(file);
    parse_spec(reader).map_err(SpecError::ParsingError)
}


#[cfg(test)]
mod tests {
    use super::*;
    use crate::header::*;

    #[test]
    fn test_header_matcher_simple_form() {
        let input = r#"
            request:
                method: GET
                url: /hello/world
            response:
                status: 200
                body: TEST
                headers:
                  - Content-Type: application/text

        "#;
        let spec = parse_spec(input.as_bytes()).expect("Failed to parse");        
        assert_eq!(spec.request.method, "GET");
        assert_eq!(spec.request.url, "/hello/world");
        assert_eq!(spec.response.status, 200);
        assert_eq!(spec.response.body, "TEST");
        assert_eq!(spec.response.headers, vec![HeaderSpec("Content-Type".to_string(), "application/text".to_string())])
    }
}

